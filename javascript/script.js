$(document).ready(function () {
    const $burgerBtn = $('.b-burger');

    $burgerBtn.on('click', function () {
        const $navMenu = $('.b-nav');
        $navMenu.toggleClass('b-nav--active');
        $burgerBtn.toggleClass('b-burger--active');
    });

    $('.js-slider').slick({
        autoplay: true,
        autoplaySpeed: 4000,
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: true,
        fade: true,
        speed: 1500,
        dots: true,
        arrows: false,
        adaptiveHeight: true,
        responsive: [
            {
                breakpoint: 767,
                settings: {
                    infinite: false,
                    autoplay: false,
                    arrows: true,
                    dots: false,
                    appendArrows: $('.js-slider-arrows')
                }
            }
        ]
    });
});

ymaps.ready(function () {
    const mapUlsk = new ymaps.Map('mapUlsk', {
        center: [54.32292685041397, 48.3964458816147],
        zoom: 18,
    }, {
        searchControlProvider: 'yandex#search'
    }),

        placemarkUlsk = new ymaps.Placemark(mapUlsk.getCenter(), {
            hintContent: 'Офис в России',
            balloonContent: 'Описание офиса в России'
        }, {
            iconLayout: 'default#image',
            iconImageHref: 'images/logo/marker-logo.png',
            iconImageSize: [50, 50]
        });

    const mapMinsk = new ymaps.Map('mapMinsk', {
        center: [53.911594623625845, 27.454577542327883],
        zoom: 18,
    }, {
        searchControlProvider: 'yandex#search'
    }),

        placemarkMinsk = new ymaps.Placemark(mapMinsk.getCenter(), {
            hintContent: 'Офис в России',
            balloonContent: 'Описание офиса в Белоруси'
        }, {
            iconLayout: 'default#image',
            iconImageHref: 'images/logo/marker-logo.png',
            iconImageSize: [50, 50]
        });

    mapUlsk.behaviors.disable('scrollZoom');
    mapMinsk.behaviors.disable('scrollZoom');

    mapUlsk.geoObjects.add(placemarkUlsk);
    mapMinsk.geoObjects.add(placemarkMinsk);
});
